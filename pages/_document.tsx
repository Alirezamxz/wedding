import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
        <title>THE WEDDING OF MOHAMMADREZA & MARYAM</title>
        <meta name="title" content="THE WEDDING OF MOHAMMADREZA & MARYAM" />
        <meta name="description" content="" />

        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content="https://369.storage.iran.liara.space/bg.jpg"
        />
        <meta
          property="og:title"
          content="THE WEDDING OF MOHAMMADREZA & MARYAM"
        />
        <meta property="og:description" content="" />
        <meta
          property="og:image"
          content="https://369.storage.iran.liara.space/bg.jpg"
        />

        <meta
          name="twitter:title"
          content="THE WEDDING OF MOHAMMADREZA & MARYAM"
        />
        <meta name="twitter:description" content="" />
        <meta
          name="twitter:image"
          content="https://369.storage.iran.liara.space/bg.jpg"
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
