"use client";

import {
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  useDisclosure,
} from "@nextui-org/react";
import axios from "axios";
import { useEffect, useRef, useState } from "react";
import toast from "react-hot-toast";
// @ts-ignore
import { Howl, Howler } from "howler";

export default function Home() {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  // SUBMIT USER
  function validateIranianPhoneNumber(number: any) {
    // Remove any leading or trailing whitespace
    number = number.trim();

    // Check if the number starts with the appropriate prefix
    if (number.startsWith("0")) {
      // Remove the prefix to get the actual digits
      let digits = number.slice(1);

      // Check the length of the digits
      if (digits.length === 10) {
        // Check if the first two digits are 09
        if (digits.startsWith("9")) {
          // Valid Iranian phone number
          return true;
        }
      }
    }

    // Not a valid Iranian phone number
    return false;
  }

  function persianToEnglishDigits(input: any) {
    const persianDigits = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    const englishDigits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

    for (let i = 0; i < persianDigits.length; i++) {
      input = input.replace(
        new RegExp(persianDigits[i], "g"),
        englishDigits[i]
      );
    }

    return input;
  }

  const handleSubmitForm = async (e: any) => {
    e.preventDefault();

    const formdata = new FormData(e.target);

    if (!formdata.get("name")) {
      toast.error("لطفا نام و نام خانوادگی را وارد کنید");
      return;
    } else if (!formdata.get("phoneNumber")) {
      toast.error("لطفا شماره موبایل را وارد کنید");
      return;
    } else if (
      !validateIranianPhoneNumber(
        persianToEnglishDigits(formdata.get("phoneNumber"))
      )
    ) {
      toast.error("لطفا شماره موبایل معتبر وارد کنید");
      return;
    }

    setIsLoading(true);

    try {
      const res = await axios.post(
        "/api/user",
        {
          name: formdata.get("name"),
          phoneNumber: persianToEnglishDigits(formdata.get("phoneNumber")),
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      setIsLoading(false);

      if (res.data.success) {
        toast.success("مشخصات برای شما ارسال شد");
        onOpenChange();
      } else {
        toast.error("شما قبلا اطلاعات را دریافت کردید");
        onOpenChange();
      }
    } catch (error) {
      console.error(error);
    }
  };

  // TEMP
  // const audioRef: any = useRef(null);
  // useEffect(() => {
  //   const handleInteraction = () => {
  //     if (audioRef.current) {
  //       audioRef.current
  //         .play()
  //         .then(() => {
  //           console.log("Audio played successfully.");
  //         })
  //         .catch((error: any) => {
  //           console.error("Failed to play audio:", error);
  //         });
  //     }
  //     // Remove the event listener after interaction to prevent multiple plays
  //     document.removeEventListener("mousedown", handleInteraction);
  //   };

  //   // Add a mousedown event listener to initiate audio playback
  //   document.addEventListener("mousedown", handleInteraction);

  //   return () => {
  //     // Cleanup: Remove the event listener on unmount
  //     document.removeEventListener("mousedown", handleInteraction);
  //   };
  // }, []);

  useEffect(() => {
    const element: any = document.querySelector("body");

    const touchEvent = new TouchEvent("touchstart", {
      bubbles: true,
      cancelable: true,
      touches: [
        new Touch({
          identifier: Date.now(),
          target: element,
          clientX: 100,
          clientY: 100,
        }),
      ],
      targetTouches: [],
      changedTouches: [
        new Touch({
          identifier: Date.now(),
          target: element,
          clientX: 100,
          clientY: 100,
        }),
      ],
      shiftKey: false,
    });

    element.dispatchEvent(touchEvent);
  }, []);

  useEffect(() => {
    var sound = new Howl({
      src: ["/music.mp3"],
      autoplay: true,
    });

    sound.once("load", function () {
      sound.play();
    });
  }, []);

  return (
    <div className="h-screen bg-[#f2f1ed] -z-40">
      <div className="h-screen border-l border-r border-[#c3aa94] mx-8">
        <div className="w-4/5 flex flex-col items-center mx-auto py-10">
          <img src="/heading.png" className="w-full mb-10" alt="" />
          <img src="/center.png" className="w-full mb-5" alt="" />
          <button
            onClick={() => {
              onOpen();
            }}
            className="animate-pulse-fast py-4 px-10 w-max border-2 mb-5 border-[#c3aa94] text-[#ae8c57] transform scale-[0.54] text-2xl font-semibold rounded-full"
          >
            مشاهده زمان و آدرس
          </button>
          <p className="text-[#ae8c57] text-center text-base font-normal">
            حضور شما در مراسم ازدواج ما باعث خوشحالیه
          </p>
        </div>
      </div>
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        placement="bottom-center"
        backdrop="blur"
      >
        <ModalContent className="bg-[#f2f1ed] font-vazir" dir="rtl">
          {(onClose) => (
            <>
              <form onSubmit={handleSubmitForm}>
                <ModalBody className="pt-8 space-y-2">
                  <Input
                    type="text"
                    placeholder="نام و نام خانوادگی"
                    variant="faded"
                    labelPlacement="inside"
                    name="name"
                  />
                  <Input
                    type="text"
                    placeholder="شماره موبایل"
                    variant="faded"
                    labelPlacement="inside"
                    name="phoneNumber"
                  />
                </ModalBody>
                <ModalFooter className="pb-5">
                  <button
                    disabled={isLoading}
                    type="submit"
                    className="py-3 px-5 w-full border-2 border-[#c3aa94] text-[#ae8c57] text-sm font-semibold rounded-full"
                  >
                    {isLoading ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24px"
                        height="24px"
                        viewBox="0 0 16 16"
                        fill="none"
                        className="animate-spin mx-auto"
                      >
                        <g
                          fill="#ae8c57"
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                        >
                          <path
                            d="M8 1.5a6.5 6.5 0 100 13 6.5 6.5 0 000-13zM0 8a8 8 0 1116 0A8 8 0 010 8z"
                            opacity=".2"
                          />

                          <path d="M7.25.75A.75.75 0 018 0a8 8 0 018 8 .75.75 0 01-1.5 0A6.5 6.5 0 008 1.5a.75.75 0 01-.75-.75z" />
                        </g>
                      </svg>
                    ) : (
                      "دریافت زمان و آدرس"
                    )}
                  </button>
                </ModalFooter>
              </form>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
}
