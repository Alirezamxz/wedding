import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { NextUIProvider } from "@nextui-org/react";
import { Toaster } from "react-hot-toast";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <NextUIProvider>
      <div className="font-vazir">
        <Toaster />
      </div>
      <Component {...pageProps} />
    </NextUIProvider>
  );
}
