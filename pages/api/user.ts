import prisma from "../../lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import { MessageWay } from "messageway";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") {
    res.status(405).send({ message: "Only POST requests allowed" });
    return;
  }

  const message = new MessageWay(process.env.MSGWAY_API_KEY as string);

  const checkuser = await prisma.user.findFirst({
    where: {
      phoneNumber: req.body.phoneNumber,
    },
  });

  if (checkuser) {
    res
      .status(200)
      .json({ success: false, message: "شما قبلا شماره ثبت کردید" });
    return;
  }

  const user = await prisma.user.create({
    data: {
      name: req.body.name,
      phoneNumber: req.body.phoneNumber,
    },
  });

  message
    .sendSMS({
      mobile: req.body.phoneNumber,
      templateID: 8664,
      length: 4,
    })
    .then(async () => {
      await prisma.user.update({
        where: {
          phoneNumber: req.body.phoneNumber,
        },
        data: {
          isSent: true,
        },
      });
    });

  res.status(200).json({ success: true, user });
}
